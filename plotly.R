# install.packages("plotly")
library(ggplot2)
library(plotly)

# example to use plotly and ggplot2 together
p <- ggplot(mpg, aes(x=displ, 
                     y=hwy, 
                     color=class)) +
  geom_point(size=3) +
  labs(x = "Engine displacement",
       y = "Highway Mileage",
       color = "Car Class")

ggplotly(p)

# install.packages("gapminder")
library(gapminder)
data <- head(gapminder, n=50)
plot <- gapminder %>%
  plot_ly(
    x = ~gdpPercap, 
    y = ~lifeExp, 
    # marker = list(size = 10,
    #               color = 'rgba(255, 182, 193, .9)',
    #               line = list(color = 'rgba(152, 0, 0, .8)',width = 2)),
    size = ~pop, 
    color = ~continent, 
    frame = ~year, 
    type = 'scatter',
    mode = 'markers',
    #symbol = ~continent, symbols = c('circle','x','o'),
    text = ~paste("Country: ", country, 
                  '<br>Population:', pop)
  )
plot <- plot %>% 
  layout(title = 'Styled Scatter',xaxis = list(type = "log"))
plot

# use add trace instead to create plot 2
plot2 <- data %>%
  plot_ly(x = ~gdpPercap)
plot2 <- plot2 %>% add_trace(y = ~lifeExp, name = 'lifeExp',mode = 'markers')
plot2
